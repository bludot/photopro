<?php
$host = $_POST['host'];
$database = $_POST['database'];
$username = $_POST['username'];
$pass = $_POST['pass'];

function dirToArray($dir) {

   $result = array();

   $cdir = scandir($dir);
   foreach ($cdir as $key => $value)
   {
      if (!in_array($value,array(".","..")))
      {
         if (is_dir($dir . DIRECTORY_SEPARATOR . $value))
         {
            $result[$value] = dirToArray($dir . DIRECTORY_SEPARATOR . $value);
         }
         else
         {
            $result[] = $value;
         }
      }
   }

   return $result;
}
function xcopy($source, $dest, $content_only = false, $permissions = 0755)
{
    // Check for symlinks
    if (is_link($source)) {
        return symlink(readlink($source), $dest);
    }

    // Simple copy for a file
    if (is_file($source)) {
        return copy($source, $dest);
    }

    // Make destination directory
    if (!is_dir($dest) && !$content_only) {
        mkdir($dest, $permissions);
    }

    // Loop through the folder
    $dir = dir($source);
    while (false !== $entry = $dir->read()) {
        // Skip pointers
        if ($entry == '.' || $entry == '..') {
            continue;
        }

        // Deep copy directories
        xcopy("$source/$entry", "$dest/$entry");
    }

    // Clean up
    $dir->close();
    return true;
}
function find_root($cur) {
	if(!is_dir($cur."public_html")) {
		/*echo '../'.$cur."<br />";*/
		return find_root('../'.$cur);
	} else {
		return $cur;
	}
}
function deleteDir($path) {
    return is_file($path) ?
            @unlink($path) :
            array_map(__FUNCTION__, glob($path.'/*')) == @rmdir($path);
}
//var_dump(dirToArray("$software_root/app/"));
//echo "$software_root/app/";
	$root_dir = find_root('');
	$software_root = (string)"./..";
function install_files() {
	$root_dir = find_root('');
	$software_root = (string)"./..";
	mkdir($software_root.'/photopro');
	xcopy($software_root."/app", $software_root.'/photopro/app/');
	xcopy("$software_root/bootstrap/", $software_root.'/photopro/bootstrap/');
	xcopy("$software_root/vendor/", $software_root.'/photopro/vendor/');
	$root_files = array(
		'.gitattributes',
		'.gitignore',
		'artisan',
		'composer.json',
		'composer.lock',
		'composer.phar',
		'CONTRIBUTING.md',
		'phpunit.xml',
		'readme.md',
		'server.php'
	);
	foreach($root_files as $root_file) {
		copy($software_root.'/'.$root_file, $root_dir.'photopro/'.$root_file);
	}
	deleteDir($software_root."/app");
	deleteDir($software_root."/bootstrap");
	deleteDir($software_root."/vendor");
	xcopy($software_root."/public", "./../", false);
	deleteDir($software_root."/public");
}

$file = <<<EOT
<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| PDO Fetch Style
	|--------------------------------------------------------------------------
	|
	| By default, database results will be returned as instances of the PHP
	| stdClass object; however, you may desire to retrieve records in an
	| array format for simplicity. Here you can tweak the fetch style.
	|
	*/

	'fetch' => PDO::FETCH_CLASS,

	/*
	|--------------------------------------------------------------------------
	| Default Database Connection Name
	|--------------------------------------------------------------------------
	|
	| Here you may specify which of the database connections below you wish
	| to use as your default connection for all database work. Of course
	| you may use many connections at once using the Database library.
	|
	*/

	'default' => 'mysql',

	/*
	|--------------------------------------------------------------------------
	| Database Connections
	|--------------------------------------------------------------------------
	|
	| Here are each of the database connections setup for your application.
	| Of course, examples of configuring each database platform that is
	| supported by Laravel is shown below to make development simple.
	|
	|
	| All database work in Laravel is done through the PHP PDO facilities
	| so make sure you have the driver for your particular database of
	| choice installed on your machine before you begin development.
	|
	*/

	'connections' => array(

		'sqlite' => array(
			'driver'   => 'sqlite',
			'database' => __DIR__.'/../database/production.sqlite',
			'prefix'   => '',
		),

		'mysql' => array(
			'driver'    => 'mysql',
			'host'      => '$host',
			'database'  => '$database',
			'username'  => '$username',
			'password'  => '$pass',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
		),

		'pgsql' => array(
			'driver'   => 'pgsql',
			'host'     => 'localhost',
			'database' => 'forge',
			'username' => 'forge',
			'password' => '',
			'charset'  => 'utf8',
			'prefix'   => '',
			'schema'   => 'public',
		),

		'sqlsrv' => array(
			'driver'   => 'sqlsrv',
			'host'     => 'localhost',
			'database' => 'database',
			'username' => 'root',
			'password' => '',
			'prefix'   => '',
		),

	),

	/*
	|--------------------------------------------------------------------------
	| Migration Repository Table
	|--------------------------------------------------------------------------
	|
	| This table keeps track of all the migrations that have already run for
	| your application. Using this information, we can determine which of
	| the migrations on disk haven't actually been run in the database.
	|
	*/

	'migrations' => 'migrations',

	/*
	|--------------------------------------------------------------------------
	| Redis Databases
	|--------------------------------------------------------------------------
	|
	| Redis is an open source, fast, and advanced key-value store that also
	| provides a richer set of commands than a typical key-value systems
	| such as APC or Memcached. Laravel makes it easy to dig right in.
	|
	*/

	'redis' => array(

		'cluster' => false,

		'default' => array(
			'host'     => '127.0.0.1',
			'port'     => 6379,
			'database' => 0,
		),

	),

);
EOT;
//install_files();
file_put_contents($software_root.'/photopro/app/config/database.php', $file);
xcopy($software_root."/public", "./../", false);
deleteDir($software_root."/public");
?>