<?php
class AdminController extends BaseController

{
	/*
	|--------------------------------------------------------------------------
	| Default Admin Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function __construct()
    {
        $this->beforeFilter('auth');
    }

	public	function getIndex()
	{
		return View::make('admin.main');
	}

    public function getUpload() {
        return View::make('admin.upload')->with('date', date('Y-m-d'));
    }

    public function postSubmit() {
        ini_set('upload_max_filesize', '200M');

        //foreach(Input::file('image') as $image) {
        $image = Input::file('image');

            $imagename = preg_replace('/\s/', "_", Input::get('filename'));
            if(strlen(Input::get('folder')) > 0) {
            $destinationPath = 'images/'.preg_replace('/\s/', "_", Input::get('folder')).'/';
            } else {
                $destinationPath = 'images/'.date('Y-m-d').'/';
            }
            if(!file_exists($destinationPath)) {
                File::makeDirectory($destinationPath, 0777, true, true);
            }

            $tmp_name = substr($imagename, 0, strlen($imagename)-strlen($image->getClientOriginalExtension())-1);
            Image::make($image->getRealPath())
//            ->save($destinationPath.$tmp_name.'1-foo-original.'.$image->getClientOriginalExtension())
            // thumbnail
            //->fit('200', '200')
            ->save($destinationPath.$tmp_name.'_thumbnail.'.$image->getClientOriginalExtension())
            // resize
            ->resize(200, null, function ($constraint) {
    $constraint->aspectRatio();
}) // set true if you want proportional image resize
            ->save($destinationPath.$tmp_name.'_resized.'.$image->getClientOriginalExtension())
            ->destroy();

            $uploadflag = $image->move($destinationPath, $tmp_name.'.'.$image->getClientOriginalExtension());

            if($uploadflag) {
                $uploadedimages[] = $tmp_name.'.'.$image->getClientOriginalExtension();
            }
        //}

        return Response::json(['success'=>true, 'message'=>'Image uploaded!', 'images'=> $uploadedimages]);
    }

    public function getFilemgr() {
        return View::make('filemgr');
    }
}