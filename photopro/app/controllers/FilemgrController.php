<?php

class FilemgrController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//

		return Response::json(['success'=>true, 'dirs'=>File::directories('./images'), 'files'=>File::files('./images'), 'prev_loc'=> './images']);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($dir)
	{
		//
		$dir = rawurldecode($dir);
		$dir = preg_replace('/,/', '/', $dir);
		$prev = explode('/', $dir);
		array_pop($prev);
		$prev = implode('/', $prev);
		$prev = './images/'.$prev;
		return Response::json(['success'=>true, 'dirs'=>File::directories('./images/'.$dir), 'files'=>File::files('./images/'.$dir), 'prev_loc'=> $prev]);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($file, $name, $type)
	{
		//
		$file = preg_replace('/,/i', "/", $file);
		$name = preg_replace('/,/i', "/", $name);
		if($type == "file") {
		    //$name = explode('.', $name);
		    //$ext = $name[count($name)-1];
		    //array_pop($name);
		    //$name = implode('.', $name);
		}
		$newname = explode('/', $file);
		array_pop($newname);
		$newname = implode('/', $newname);
		$file = explode('/', $file);
		$file_name = $file[count($file)-1];
		$file_name = explode('.', $file_name);
		    $ext = $file_name[count($file_name)-1];
		    array_pop($file_name);
		    $file_name = implode('.', $file_name);
        array_pop($file);

		$file_before = implode('/', $file);

		$file_name = explode('_', $file_name);
		array_pop($file_name);
		$file_name = implode('_', $file_name);

		if($type == "file") {
		    $tfile = $file_before.'/'.$file_name.'_resized.'.$ext;
		    $nname = $name.'_resized.'.$ext;
		    $tnewname = $newname."/".$nname;
		    File::move($tfile, $tnewname);
		    $tfile = $file_before.'/'.$file_name.'_thumbnail.'.$ext;
		    $nname = $name.'_thumbnail.'.$ext;
		    $tnewname = $newname."/".$nname;
		    File::move($tfile, $tnewname);
		    $tfile = $file_before.'/'.$file_name.'.'.$ext;
		    $nname = $name.'.'.$ext;
		    $tnewname = $newname."/".$nname;
		    File::move($tfile, $tnewname);
		} else {

		}
		return Response::json(['success'=>true, 'file'=>$file, 'name'=>$nname, 'newname'=>$tnewname]);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($name)
	{
		//
		$name = preg_replace('/,/', "/", $name);
		if(File::isDirectory($name)) {
		    File::deleteDirectory($name, true);
		    File::deleteDirectory($name);
		} else {
		    $name = explode('_resized', $name);
		    $name = implode('', $name);
		    $res = File::delete($name);
		    $name = explode('.', $name);
		    $ext = $name[count($name)-1];
		    array_pop($name);
		    $name_beg = implode('.', $name);
		    $nname = $name_beg.'_thumbnail.'.$ext;
		    $res2 = File::delete($nname);
		    $nname = $name_beg.'_resized.'.$ext;
		    $res2 = File::delete($nname);
		}
		return Response::json(['success'=>true, 'file'=>$name, 'more'=>$nname]);
	}


}