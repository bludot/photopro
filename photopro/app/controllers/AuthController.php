<?php

class AuthController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

    public function __construct()
    {
        $this->beforeFilter('auth', array('except' => array('getIndex', 'postAuth')));
    }

	public function getIndex() {
	    if(Auth::check()) {
	        return Redirect::to('/admin');
	    }
		return View::make('login');
	}
	public function postAuth()
	{
       	if(Auth::attempt(array('username' => Input::get('user'), 'password' => Input::get('pass')), true)) {
       	    return Redirect::to('/admin');
       	} else {
       	    return Response::json(['success'=>false, 'message'=> 'fail']);
       	}
	}
	
	public function getUpdatepass() {
    	return View::make('update_pass');
	}
	
	public function postUpdate() {
    	//DB::update('update users set password = ? where name = ?', array(Input::get('pass'), );
    	//$user = UserEventbot::findOrFail(Auth::user()->id);
    	$user = Auth::user();

        $user->password = Hash::make(Input::get('pass'));
        $user->save();
        return Redirect::to('/admin');
	}
}