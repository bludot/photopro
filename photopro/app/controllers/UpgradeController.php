<?php
class UpgradeController extends BaseController

{
	/*
	|--------------------------------------------------------------------------
	| Default Admin Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function __construct()
    {
        $this->beforeFilter('auth');
    }

    public function getVersion() {
        return $this->version()->get()->toArray()[0]['version'];
        //var_dump($data);
        //return Response::json(['version' => $data[0]['version']]);
    }

	public	function getIndex()
	{
	    $updated = false;
	    $version = json_decode(file_get_contents('http://vps.bludotos.com/photopro/version'));
	    if($version->version > $this->getVersion()) {
	        $ver = explode('.', $this->getVersion());
            $vern = intval($ver[1]);
            $vern += 1;
            $ver = $ver[0].".".$vern.".0";
	        $res = $this->downloadFile('http://vps.bludotos.com/photopro/upgrade_'.$ver.'.zip', storage_path().'/upgrade/upgrade.zip');
	        if($res) {
	            $this->install();
	            File::delete(storage_path().'/upgrade/upgrade.zip');
	        }
	        if($res) {
	            $updated = true;
	            $tv_id = $this->version()->get()->toArray()[0]['id'];
	            //var_dump($tv_id);
	            $tv = Version::find($tv_id);
	            $tv->current = 0;
	            $mversion = new Version;
	            $mversion->version = (string)$version->version;
	            $mversion->current = 1;
	            //var_dump($this->version());
	            $mversion->save();
	            $tv->save();
	        }
	    }
	    if(!isset($res)) {
	        $res = true;
	    }
		return Response::json(['success' => $res, 'new_version'=>$version, 'current_version'=> $this->getVersion(), 'updated'=>$updated]);
	}
    public function getBeta() {
	    $beta_ = 'beta/';
	    $updated = false;
	    $version = json_decode(file_get_contents('http://vps.bludotos.com/photopro/'.$beta_.'version'));
	    if($version->version > $this->getVersion()) {
	        $ver = explode('.', $this->getVersion());
            $vern = intval($ver[2]);
            $vern += 1;
            $ver = $ver[0].".".$ver[1].".".$vern;
	        $res = $this->downloadFile('http://vps.bludotos.com/photopro/'.$beta_.'upgrade_'.$ver.'.zip', storage_path().'/upgrade/upgrade.zip');
	        if($res) {
	            $this->install();
	            File::delete(storage_path().'/upgrade/upgrade.zip');
	        }
	        if($res) {
	            $updated = true;
	            $tv_id = $this->version()->get()->toArray()[0]['id'];
	            //var_dump($tv_id);
	            $tv = Version::find($tv_id);
	            $tv->current = 0;
	            $mversion = new Version;
	            $mversion->version = (string)$version->version;
	            $mversion->current = 1;
	            //var_dump($this->version());
	            $mversion->save();
	            $tv->save();
	        }
	    }
	    if(!isset($res)) {
	        $res = true;
	    }
		return Response::json(['success' => $res, 'new_version'=>$version->version, 'current_version'=> $this->getVersion(), 'updated'=>$updated]);
	}
    private function downloadFile ($url, $path) {

      $newfname = $path;
      $file = fopen ($url, "rb");
      if ($file) {
        $newf = fopen ($newfname, "wb");

        if ($newf)
        while(!feof($file)) {
          fwrite($newf, fread($file, 1024 * 8 ), 1024 * 8 );
        } else {
          return false;
        }
      } else {
        return false;
      }

      if ($file) {
        fclose($file);
      } else {
        return false;
      }

      if ($newf) {
        fclose($newf);
      } else {
        return false;
      }
      return true;
     }
     private function install() {
        $zip = new ZipArchive;
        $res = $zip->open(storage_path().'/upgrade/upgrade.zip');
        if ($res === TRUE) {
          $zip->extractTo(storage_path().'/../../../');
          $zip->close();
          if(File::exists(storage_path().'/upgrade/script.php')) {
            include(storage_path().'/upgrade/script.php');
          }
        } else {
        }
     }
     private function version() {
        $data = Version::where('current', '=', 1);
        return $data;
     }
}