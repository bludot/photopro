<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'HomeController@Index');
Route::controller('/showcase', 'ShowcaseController');

Route::controller('/admin', 'AdminController');

Route::resource('/_admin/filemgr', 'FilemgrController');

Route::get('/_admin/filemgr/{file}/{name}/{type}/edit', array('as' => 'editFilemgr', 'uses' => 'FilemgrController@edit'));
Route::get('/_admin/filemgr/{name}/destroy', array('as' => 'destroyFilemgr', 'uses' => 'FilemgrController@destroy'));

Route::controller('/auth', 'AuthController');
Route::controller('/_admin/upgrade', 'UpgradeController');

Route::controller('/_admin/plugins', 'PluginController');

Route::get('/logout', function() {
Auth::logout();
return Redirect::guest('/');
});