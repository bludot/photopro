@extends('frame_layout')

@section('frame_content')

<form class="uploader" action="" enctype="multipart/form-data">
    Folder:
    <input class="input" name="folder" id="folder" value="{{$date}}" />
    <input id="images" type="file" name="image[]" multiple>
    <button type="submit" class="button" >Upload</button>
</form>
<div id="upload-text">
Click or Drag &amp; Drop Image(s) here
</div>
<div id="preview">
<div id="upload-click"></div>
</div>
<script>
//    var backup;
    var files = [];
    var previewer = document.querySelector("#preview");
    var upload = function(file, rand) {
    console.log(rand);
            //upload the file
            var xhr = new Array();
            xhr[rand] = new XMLHttpRequest();
            xhr[rand].open("post", "/admin/submit");

            xhr[rand].upload.addEventListener("progress", function(event) {
                console.log(event);
                if (event.lengthComputable) {
                    previewer.children[rand].querySelector('.loader').children[0].style.width = (event.loaded / event.total) * 100 + "%";
                    //$(".preview[id='"+rand+"'] .updone").html(((event.loaded / event.total) * 100).toFixed(2)+"%");
                } else {
                    alert("Failed to compute file upload length");
                }
            }, false);

            xhr[rand].onreadystatechange = function(oEvent) {
                if (xhr[rand].readyState === 4) {
                    if (xhr[rand].status === 200) {
                        //$(".progress[id='"+rand+"'] span").css("width","100%");
                        //$(".preview[id='"+rand+"']").find(".updone").html("100%");
                        //$(".preview[id='"+rand+"'] .overlay").css("display","none");
                        previewer.children[rand].querySelector('.loader').className += ' complete';
                        previewer.children[rand].querySelector('.loader').querySelector('div').className = 'fontawesome-ok-sign';
                    } else {
                        alert("Error : " + xhr[rand].statusText);
                    }
                }
            };

            // Set headers
            //    xhr[rand].setRequestHeader("Content-Type", "multipart/form-data");
            //    xhr[rand].setRequestHeader("X-File-Name", file.fileName);
            //    xhr[rand].setRequestHeader("X-File-Size", file.fileSize);
            //    xhr[rand].setRequestHeader("X-File-Type", file.type);

            // Send the file (doh)
            var filedata = new FormData();
            filedata.append('image', file);
            filedata.append('folder', form.querySelector('#folder').value);
            filedata.append('filename', previewer.children[rand].querySelector('input').value+'.'+previewer.children[rand].querySelector('input').ext);

            xhr[rand].send(filedata);
        }
        //- See more at: http://www.amitpatil.me/drag-and-drop-multiple-file-upload-with-progress-bar/#sthash.jzdhdNBe.dpuf
    var form = document.querySelector('form');
    //var request = new XMLHttpRequest();

    //request.upload.addEventListener('progress', function(e) {
    //    console.log(e.loaded/e.total*100+'%');
    //}, false);

    form.addEventListener('submit', function(e) {
        e.preventDefault();

        //var formdata = new FormData(this);
        //var files = this.querySelector('#images').files;
        var num = 1;
        for (var i = 0; i < files.length; i++) {
            for(var j = 0; j < files[i].length; j++) {
                upload(files[i][j], (num));
                num++;
            }
        }

        //request.open('POST', '/admin/submit');
        //request.send(formdata);


    }, false);

    function readImage(file) {
        var reader = new FileReader();
        var image = new Image();
        var img_cont = document.createElement('div');
        img_cont.className = 'image';
        var img = document.createElement('img');
        var loader = document.createElement('div');
        loader.className = 'loader';
        loader.bar = document.createElement('div');
        loader.appendChild(loader.bar);
        var name = document.createElement('input');
        name.type = 'text';
        name.name = 'filename';

        img_cont.appendChild(img);
        img_cont.appendChild(loader);
        img_cont.appendChild(name);
        previewer.appendChild(img_cont);
        reader.readAsDataURL(file);
        reader.onload = function(_file) {
            image.src = _file.target.result; // url.createObjectURL(file);
            image.onload = function() {
                var w = this.width,
                    h = this.height,
                    t = file.type.split('/')[1], // ext only: // file.type.split('/')[1],
                    n = file.name,
                    s = ~~(file.size / 1024) + 'KB';
                img.src = this.src;
                name.value = n.substr(0, (n.length-t.length-1));
                name.addEventListener('click', function(e) {
                    var e = e || window.event;
                    e.preventDefault();
                    return false;
                }, false);
                name.ext = t;
            };
            image.onerror = function() {
                alert('Invalid file type: ' + file.type);
            };
        };

    }

    form.querySelector('#images').addEventListener('change', function(e) {
        files.push(this.files);
        var file = this.files;
        for (var i = 0; i < file.length; i++) {
            readImage(file[i]);
        }
    }, false);
    var handleDragOver = function(e) {

        var e = e || window.event;
        e.stopPropagation();
        e.preventDefault();
//        form.querySelector('#images').style.zIndex = 1;
        previewer.style.background = 'rgba(3, 121, 236, 0.80)';
        previewer.style.border = '5px dashed #bbb';
    };
    var handleDragLeave = function() {
//        form.querySelector('#images').style.zIndex = 1;
        previewer.style.background = 'transparent';
        previewer.style.border = '5px dashed transparent';
    }
    var draginput = function(e) {
        var e = e || window.event;
        e.stopPropagation();
        e.preventDefault();
        var files = e.dataTransfer.files;
        form.querySelector('#images').files = files;
        handleDragLeave();
    }

    previewer.addEventListener('dragover', handleDragOver, false);
    previewer.addEventListener('dragleave', handleDragLeave, false);
    previewer.addEventListener('drop', draginput, false);
    previewer.querySelector('#upload-click').addEventListener('click', function() {
        form.querySelector('#images').click();
    }, false);
</script>

@stop