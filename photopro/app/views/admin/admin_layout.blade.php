<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Admin</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link type="text/css" rel="stylesheet" href="/css/main.css">
</head>
<body>
    <header>
        <span class="version"><?php
        echo "Ver: ".Version::where('current', '=', 1)->get()->toArray()[0]['version'];
        ?></span>
    </header>
	<main class="admin">
    	@yield('content')
	</main>
</body>
</html>