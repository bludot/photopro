@extends('frame_layout')

@section('frame_content')
    <div class="toolbar">
        <span class="fontawesome-angle-left back"></span>
    </div>
    <div id="viewer" class="content">
    </div>
    <script>
        var ajax = function(data) {
        var data = data;
            var ajax = new XMLHttpRequest();
            ajax.open(data.method, data.url, data.sync);
            if(data.method = 'post') {
                ajax.setRequestHeader("Content-Type", "multipart/form-data");
            }
            ajax.onreadystatechange = function() {
                if(data.states["_"+ajax.readyState]) {
                    data.states["_"+ajax.readyState](ajax);
                }
            }
            ajax.send(data.send);
        }

        var go = function(url) {
        console.log(url);
        if(url != '.') {
            url = url.replace(/\//g, ',');
            url = '/'+encodeURIComponent(url.substr(9));
        } else {
            url = '';
        }
        ajax({
            method: 'get',
            url: '/_admin/filemgr'+url,
            sync: true,
            states: {
                '_4': function(ajax) {
                    var data = JSON.parse(ajax.responseText);
                    display(data);
                }
            }
        });
        }
        var display = function(data) {
            document.querySelector('.toolbar').querySelector('.back').data = {
                url: data.prev_loc
            };
            document.querySelector('#viewer').innerHTML = '';
            var load_img = function() {
                this.style.background = 'url('+this.img+') no-repeat center';
                this.style.backgroundSize = 'contain';
                this.className = 'file_img';
                this.removeEventListener('click', load_img, false);
            };

            var edit_name = function() {
                var inputc = document.createElement('div');
                inputc.data = this.data;
                var input = document.createElement('input');
                inputc.appendChild(input);
                var cancel = document.createElement('div');
                cancel.className = 'cancel fontawesome-remove-sign';
                var check = document.createElement('div');
                check.className = 'check fontawesome-ok-sign';
                inputc.appendChild(cancel);
                inputc.appendChild(check);
                check.addEventListener('click', change_name, false);
                cancel.addEventListener('click', restore_name, false);
                input.value = this.data.name;
                this.parentNode.appendChild(inputc);
                this.parentNode.removeChild(this);
            }

            var restore_name = function() {
                var span = document.createElement('span');
                span.appendChild(document.createTextNode(this.parentNode.data.name));
                span.data = this.parentNode.data;
                span.addEventListener('click', edit_name, false);
                this.parentNode.parentNode.appendChild(span);
                this.parentNode.parentNode.removeChild(this.parentNode);
            }

            var change_name = function() {
                var node = this;
                ajax({
                    method: 'get',
                    url: '/_admin/filemgr/'+this.parentNode.data.full_name.replace(/\//g, ",")+'/'+this.parentNode.querySelector('input').value.replace(/\//g, ",")+'/'+this.parentNode.data.type+'/edit',
                    sync: true,
                    states: {
                        '_4': function(ajax) {
                            var data = JSON.parse(ajax.responseText);
                            console.log(data);
                            if(data.success) {
                                var span = document.createElement('span');
                                span.appendChild(document.createTextNode(node.parentNode.querySelector('input').value));
                                span.data = node.parentNode.data;
                                span.data.name = node.parentNode.querySelector('input').value;
                                span.addEventListener('click', edit_name, false);
                                node.parentNode.parentNode.appendChild(span);
                                node.parentNode.parentNode.removeChild(node.parentNode);
                            }
                        }
                    }
                });
            }
            var delete_file = function() {
                var node = this;
                ajax({
                    method: 'get',
                    url: '/_admin/filemgr/'+this.parentNode.data.full_name.replace(/\//g, ",")+'/destroy',
                    sync: true,
                    states: {
                        '_4': function(ajax) {
                            var data = JSON.parse(ajax.responseText);
                            console.log(data);
                            if(data.success) {
                                document.querySelector('#viewer').removeChild(node.parentNode.parentNode);
                                /*var span = document.createElement('span');
                                span.appendChild(document.createTextNode(node.parentNode.querySelector('input').value));
                                span.data = node.parentNode.data;
                                span.data.name = node.parentNode.querySelector('input').value;
                                span.addEventListener('click', edit_name, false);
                                node.parentNode.parentNode.appendChild(span);
                                node.parentNode.parentNode.removeChild(node.parentNode);*/
                            }
                        }
                    }
                });
            }
            for(var i in data.dirs) {
                var cont = document.createElement('div');
                cont.className = 'container';
                var opt = document.createElement('div');
                opt.className = 'opts';
                opt.delete = document.createElement('span');
                opt.delete.className = 'entypo-cancel';
                opt.appendChild(opt.delete);
                opt.delete.addEventListener('click', delete_file, false);
                var dir_img = document.createElement('div');
                dir_img.className = 'dir_img entypo-folder';
                dir_img.data = {
                    url: data.dirs[i]
                };
                var name = document.createElement('span');
                name.data = {
                    name: data.dirs[i].substr(1).split('/')[data.dirs[i].substr(1).split('/').length-1],
                    full_name: data.dirs[i],
                    type: 'dir'
                };
                opt.data = name.data;
                name.appendChild(document.createTextNode(data.dirs[i].substr(1).split('/')[data.dirs[i].substr(1).split('/').length-1]));
                name.addEventListener('click', edit_name, false);
                cont.appendChild(opt);
                cont.appendChild(dir_img);
                cont.appendChild(name);
                document.querySelector('#viewer').appendChild(cont);
                dir_img.addEventListener('click', function() {
//                    go(this.data.url.replace(/\//g, "."));
                    go(this.data.url);
                }, false);
            }
            for(var i in data.files) {
                var cont = document.createElement('div');
                cont.className = 'container';
                var opt = document.createElement('div');
                opt.className = 'opts';
                opt.delete = document.createElement('span');
                opt.delete.className = 'entypo-cancel';
                opt.appendChild(opt.delete);
                opt.delete.addEventListener('click', delete_file, false);
                var file_img = document.createElement('div');
                file_img.className = 'file_img';
                file_img.img = data.files[i].substr(1);
                if(data.files[i].substr(1).split('/')[data.files[i].substr(1).split('/').length-1].substr(-12, 12).indexOf("_resized") != -1) {
                file_img.style.background = 'url('+data.files[i].substr(1)+') no-repeat center';
                file_img.style.backgroundSize = 'contain';
                } else {
                    file_img.className += " entypo-attention error";
                    file_img.addEventListener('click', load_img, false);
                }
                var name = document.createElement('span');
                name.data = {
                    //name: data.files[i].substr(1).split('/')[data.files[i].substr(1).split('/').length-1].substr(0, data.files[i].substr(1).split('/')[data.files[i].substr(1).split('/').length-1].length-12),
                    name: data.files[i].substr(1).split('/')[data.files[i].substr(1).split('/').length-1].split('_resized')[0],
                    full_name: data.files[i],
                    type: 'file'
                };
                opt.data = name.data;
                name.appendChild(document.createTextNode(name.data.name));
                name.addEventListener('click', edit_name, false);
                cont.appendChild(opt);
                cont.appendChild(file_img);
                cont.appendChild(name);
                if(data.files[i].substr(1).split('/')[data.files[i].substr(1).split('/').length-1].substr(-12, 12).indexOf("_resized") != -1) {
                    document.querySelector('#viewer').appendChild(cont);
                }
            }
        }
        go('.');
        document.querySelector('.toolbar').querySelector('.back').addEventListener('click', function() {
            go(this.data.url);
        }, false);
    </script>
@stop