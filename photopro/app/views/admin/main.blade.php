@extends('admin/admin_layout')

@section('content')
    <aside>
        <ul>
            <li>
                <a href="/admin/?view=admin.upload" class="admin_upload">Upload</a>
            </li>
            <li>
                <a href="/admin/?view=admin.filemgr" class="admin_filemgr">File Manager</a>
            </li>
            <li>
                <a href="/admin/?view=admin.plugin" class="admin_plugin">Plugin Manager</a>
            </li>
        </ul>
    </aside>
    <!--<iframe src="/admin/filemgr">
    </iframe>-->
    @if (Input::get('view'))
    @include(Input::get('view'), array('date'=> date('Y-m-d')))
    <?php
        $view = preg_replace('/\./', '_', Input::get('view'));
    ?>
    @else
    @include('admin.upload', array('date'=> date('Y-m-d')))
    <?php
        $view = 'admin_upload';
    ?>
    @endif
    <script>
        //document.querySelector('aside').querySelector('ul').querySelector('.{{preg_replace('/\./', '_', Input::get("view"))}}').parentNode.className = 'on';
        document.querySelector('aside').querySelector('ul').querySelector('.{{$view}}').parentNode.className = 'on';
        /*var frame = document.querySelector('iframe');
        for(var i = 0; i < opts.length; i++) {
            opts[i].addEventListener('click', function() {
                frame.src = this.querySelector('a').attributes.data.value;
            }, false);
        }*/
    </script>
@stop