<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Laravel PHP Framework</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	{{ HTML::style('/css/main.css') }}
</head>
<body>
    <header>
    </header>
	<main>
    	@yield('content')
	</main>
	<footer>
	</footer>
</body>
</html>