@extends('showcase_layout')

@section('content')
    <h1>Welcome</h1>
    <article>
    <p class="large-msg">Welcome to tonynoname.com! If you want to contact me, click <a href="#">HERE</a>.</p>
    </article>
@stop

@section('images')
<?php if(isset($id)) {
    $images = array();
    $id = preg_replace('/\,/', '/', $id);
    $images = File::files('./images/'.$id.'');
     /*if ($handle = opendir('./images/'. $id .'')) {

    while (false !== ($entry = readdir($handle))) {

        if ($entry != "." && $entry != "..") {
            if(!is_dir("/images/$id/$entry")) {
                array_push($images, "/images/$id/$entry");
            }
        }
    }

    closedir($handle);
}*/
     }
     ?>
    <div class="container">
        <?php if(isset($images)) { ?>
    <?php foreach ($images as $image) {
        $image = substr($image, 1);
        $image_name = explode('/', $image);
        $image_name = $image_name[count($image_name)-1];
        $image_last = explode('.', $image_name);
//        $image_last = substr($image_last[count($image_last)-2], -10, 10);
        $image_last = substr($image_last[count($image_last)-2], -8, 8);
        //echo $image_last."<br />";
//        if($image_last == "-thumbnail") {
        if($image_last == '_resized') {
        ?>
        <div class="image"><img src="{{$image}}" onclick="show('{{$image}}', '{{$image_name}}');"/><span>{{$image_name}}</span></div>
    <?php 
    }
    }
     ?>
    <?php } ?>
    </div>
@stop

@section('carousel')
    <div class="carousel">
        <span></span>
        <div class="images">
        <?php if(isset($images)) { ?>
    @foreach ($images as $image)
        <div><img src="{{$image}}" /></div>
    @endforeach
    <?php } ?>
        </div>
        <span></span>
    </div>
@stop