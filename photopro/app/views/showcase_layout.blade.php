<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Laravel PHP Framework</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link type="text/css" rel="stylesheet" href="/css/main.css">
</head>
<body>
    <header>
    </header>
	<main>
    	@yield('images')
	</main>
	<footer>
	</footer>
	<script>
	var shim = document.createElement('div');
    shim.className = 'shim';
    document.body.appendChild(shim);

    var addShim = function() {
        shim.style.display = 'block';
        setTimeout(function() {
            shim.style.background = 'rgba(0,0,0,0.6)';
        }, 0);
    };
var removeShim = function() {
        shim.style.background = 'rgba(0,0,0,0)';
        setTimeout(function() {
            shim.style.display = 'none';
        }, 300);
    };
var cpopup = function(data) {
var popup = document.createElement('div');
        popup.style.top = '-100%';
        popup.className = 'popup';
        if (data.dimensions) {
            popup.style.width = data.dimensions.w;
            popup.style.height = data.dimensions.h;
        };

        popup.close = function(node) {
            if (!node) {
                var node = this;
            };
            node.style.top = '50px';
            var temp = node;
            setTimeout(function() {
                temp.style.top = '-100%';
                removeShim();
                setTimeout(function() {
                    document.body.removeChild(temp);
                }, 300);
            }, 150);
        };
        // close button
        var close = document.createElement('div');
        close.className = 'close-butt';
        var temp = this.parentNode;
        close.addEventListener('click', function() {
            popup.close(this.parentNode);
        }, false);
        popup.appendChild(close);

        document.body.appendChild(popup);
        setTimeout(function() {
            popup.style.top = 60 + 'px';
            addShim();
        }, 0);
        return popup;
    };
    
    var show = function(image, name) {
        var temp = cpopup({
			dimensions: {
					w: '90%',
					h: '500px'
				}
			});
			var img = document.createElement('img');
			temp.appendChild(img);
			img.style.cssText = 'position:relative;left:0;right:0;height:100%;width:auto;margin:auto;box-shadow:0px 2px 5px 0px rgba(0,0,0,0.26);';
			img.src = image.split('_resized')[0]+image.split('_resized')[1];
			img.parentNode.style.background = 'rgb(150, 150, 150)';
			img.parentNode.style.padding = '15px 0';
			img.parentNode.style.bottom = '20px';
    }
			</script>
</body>
</html>